var tempdiv = document.createElement('div');
tempdiv.setAttribute('id', 'axion_widget');
document.body.appendChild(tempdiv);

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

ReactDOM.render(
  <App />,
  document.getElementById('axion_widget')
);
