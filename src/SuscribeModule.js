import React, { Component } from 'react';
import SuscribeText from './Suscribe/SuscribeText';
import SuscribeForm from './Suscribe/SuscribeForm';
import SuscribeClose from './Suscribe/SuscribeClose';
import ResponsiveStyles from './Suscribe/ResponsiveStyles';
import GlobalStyles from './Suscribe/GlobalStyles';
var mediaquery = window.matchMedia("(max-width: 500px)");

class SuscribeModule extends Component {

  constructor(props) {
    super(props);
    // this.setInvisible = this.setInvisible.bind(this)
    if (mediaquery.matches) {
       this.state = {
        isShowing: true,
        boxBottom: '-419px',
        invisible: 'block',
        opacity: '1',
        active: false
      };
    } else {
      this.state = {
        isShowing: true,
        invisible: 'block',
        opacity: '1',
        active: false,
        boxBottom: '-60px'
        //comentar lo de arriba y descomentar esto para ver la barra de suscripción.
        // active: true,
        // boxBoxttom: '0px'
      };
    }


    var inter = setInterval(()=>{
      console.log('sscontact:',window.sscontact);
      if(window.sscontact == 0){
        clearInterval(inter);
        this.setState({ active: true });
      }
      else if(window.sscontact != 0){ //si hay un objeto contacto (distinto a 0  (por defecto))
        if(!window.sscontact['Suscripción']){ //si no hay una suscripción
          this.setState({ active: true }); // Se activa el state
        }
        clearInterval(inter); //se deja de hacer el interval cada 2'
      }
    }, 2000);
  }
  
  componentDidMount = function() {
    window.addEventListener('scroll', this.handleScroll);
  }

  handleScroll = function(event) {
    if(this.state.active){
      this.setState({
        hasScroll: true
      });
      if(this.state.hasScroll){
        window.removeEventListener('scroll', this.handleScroll);
        setTimeout(() =>{
          if (mediaquery.matches) {
            // para dispositivos moviles
            this.setState({
              boxBottom: '-60px'
            });
          }else{
            this.setState({
              boxBottom: '0px'
            });
          }
        }, 5000); //TIEMPO PARA QUE SE MUESTRE **************
      }
    }//is está active el state
  }.bind(this);

  setInvisible = function(){
    this.setState({
      opacity: '0'
    });
    setTimeout(()=>{
      this.setState({
        invisible: 'none'
      });
    }, 1000);
  }.bind(this)

  toggle = function(){
    this.setState({
      isShowing: !this.state.isShowing
    });
    if(this.state.isShowing){ //si la pestaña está arriba
      if (mediaquery.matches) {
        // para dispositivos moviles
        this.setState({
          boxBottom: '-60px'
        });
      }else{
        this.setState({
          boxBottom: '0px'
        });
      }
    }else{
      if (mediaquery.matches) {
        // para dispositivos moviles
        this.setState({
          boxBottom: '-419px'
        });
      }else{
        this.setState({
          boxBottom: '-60px'
        });
      }
    }
  }.bind(this)

  render() {
    const vistageBox = {
      bottom: this.state.boxBottom,
      opacity: this.state.opacity,
      display: this.state.invisible
    } 
    return (
      <div>
      <GlobalStyles />
      <ResponsiveStyles />
      <div style={vistageBox} id="vistageBox">
        <div className="vistageTab" onClick={this.toggle}></div>
        <div style={{width:'80%', position: 'relative', margin: '0 auto', right: 0, left: 0, marginTop: '12px'}}>
          <SuscribeText />
          <SuscribeForm />
          <SuscribeClose  setInvisible={this.setInvisible}/>
        </div>
      </div>
      </div>
    );
  }
}

export default SuscribeModule;
