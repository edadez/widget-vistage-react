import React, { Component } from 'react';
import TopZone from './TopZone';
import BottomZone from './BottomZone';
import CloseButton from './CloseButton';

class MessageBox extends Component{
	render(){
		return(
			<div>
				<div id="vw_messagebox">
					<CloseButton handlerClose={this.props.handlerClose} onClick={this.props.handlerClose} />
					<TopZone />
					<BottomZone handlerClose={this.props.handlerClose} onClick={this.props.handlerClose}/>
				</div>
			</div>
		);
	}
}

export default MessageBox;