import React, { Component } from 'react';

class GlobalStyles extends Component {
  render() {
    return (
      <style>{`
      	#vw_modalbox{
      		position: fixed !important;
          background: rgba(0,0,0,0.5) !important;
          width: 100% !important;
          height: 100vh !important;
          z-index: 99999999999 !important;
          top: 0 !important;
          left: 0 !important;
          transition: all .5s !important;
      	}
        #vw_messagebox{
          width: 47%;
          height: auto;
          background: white !important;
          border-radius: 6px !important;
          margin: 0 auto !important;
          margin-top: 3em !important;
          box-shadow: 2px 4px 16px #3c3c3c !important;
          position: relative;
        }
        #vw_topzone{
          width: 100%;
          background-color: #003f5b;
          background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='4' height='4' viewBox='0 0 4 4'%3E%3Cpath fill='%234e8aa5' fill-opacity='0.4' d='M1 3h1v1H1V3zm2-2h1v1H3V1z'%3E%3C/path%3E%3C/svg%3E");
          color: white;
          height: auto;
          padding: 1em;
          box-sizing: border-box;
          -webkit-box-sizing: border-box !important;
          -moz-box-sizing: border-box !important;
          border-top-right-radius: 4px;
          border-top-left-radius: 4px;
        }
        #vw_topzone p{
          text-align: center;
          font-size: 30px;
          padding: 1em;
          line-height: 1.3;
        }
        #vw_bottomzone{
          width: 100% !important;
          height: auto !important;
          background: white !important;
          color:#003f5b !important;
          padding: 2em !important;
          position: relative !important;
          -webkit-box-sizing: border-box !important;
          -moz-box-sizing: border-box !important;
          box-sizing: border-box !important;
          text-align: center !important;
          border-radius: 6px !important;
          display: inline-block !important;
        }
        #vw_bottomzone p{
          color: #003f5b !important;
          font-size: 18px !important;
        }
        #vw_bottomzone button{
          width: 26%;
          height: 38px ;
          border: none !important;
          margin: 0 10px;
          cursor:pointer !important;
        }
        #vw_bottomzone button.si{
          background: #003f5b;
          color: white !important;
        }
        #vw_bottomzone button.si:hover{
          background: #095678 !important;
        }
        #vw_bottomzone button.no{
          background: white;
          color: #003f5b !important;
          border: 1px solid #003f5b !important;
        }
         #vw_bottomzone button.no:hover{
          background: #efefef !important;
        }
        #vw_btnclosemodal{
          width: 20px !important;
          height: 20px!important;
          background: #003f5b !important;
          color: white !important;
          text-align: center !important;
          position: absolute !important;
          top: 0 !important;
          right: 0 !important;
          z-index: 2 !important;
          cursor:pointer !important;
          border-top-right-radius: 4px !important;
        }
        #vw_btnclosemodal p{
          margin:3.5px !important;
          padding:0 !important;
          width:12px;
          height:12px;
          background-size: cover;
          display:block !important;
          max-width: 256px;max-height: 256px;background-image: url(data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDE1LjY0MiAxNS42NDIiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDE1LjY0MiAxNS42NDIiIHdpZHRoPSIxNnB4IiBoZWlnaHQ9IjE2cHgiPgogIDxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTguODgyLDcuODIxbDYuNTQxLTYuNTQxYzAuMjkzLTAuMjkzLDAuMjkzLTAuNzY4LDAtMS4wNjEgIGMtMC4yOTMtMC4yOTMtMC43NjgtMC4yOTMtMS4wNjEsMEw3LjgyMSw2Ljc2TDEuMjgsMC4yMmMtMC4yOTMtMC4yOTMtMC43NjgtMC4yOTMtMS4wNjEsMGMtMC4yOTMsMC4yOTMtMC4yOTMsMC43NjgsMCwxLjA2MSAgbDYuNTQxLDYuNTQxTDAuMjIsMTQuMzYyYy0wLjI5MywwLjI5My0wLjI5MywwLjc2OCwwLDEuMDYxYzAuMTQ3LDAuMTQ2LDAuMzM4LDAuMjIsMC41MywwLjIyczAuMzg0LTAuMDczLDAuNTMtMC4yMmw2LjU0MS02LjU0MSAgbDYuNTQxLDYuNTQxYzAuMTQ3LDAuMTQ2LDAuMzM4LDAuMjIsMC41MywwLjIyYzAuMTkyLDAsMC4zODQtMC4wNzMsMC41My0wLjIyYzAuMjkzLTAuMjkzLDAuMjkzLTAuNzY4LDAtMS4wNjFMOC44ODIsNy44MjF6IiBmaWxsPSIjRkZGRkZGIi8+Cjwvc3ZnPgo=);
        }
        #vw_toptrampa{
          position: fixed;
          height: 8px;
          z-index: 99999999999999 !important;
          width: 100%;
          right: 0;
          top: 0;
        }
      `}</style>
    );
  }
}

export default GlobalStyles;
