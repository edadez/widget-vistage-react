import React, { Component } from 'react';
// este es un componente que agrega un div en el top del site, y cuando el usuario pasa
// el mouse por ahí se levanta la modal.
class TopTrampa extends Component{

	render(){
		return(
			<div id="vw_toptrampa" onMouseOver={this.props.mostrarModal} style={{display:this.props.ocultar}}>
			</div>
		);
	}
}

export default TopTrampa;