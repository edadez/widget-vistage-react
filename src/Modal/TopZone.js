import React, { Component } from 'react';

class TopZone extends Component{
	constructor(props) {
	  super(props);
	  this.state = {
	  	nombre: ''
	  };
	  // ESTO FUNCIONA SOLO EN UN SERVIDOR (QUE NO SEA LOCALHOST)
	  // En sharpspring queda registrado el dominio de donde se está trackeando, y localhost no vale.

	  //el window.sscontact es una variable que viene de sharpspring, que está seteada en este caso en 
	  //el código de seguimiento del header de vistage (revisar el tracking de sharpspring code en vistachile)
	  var inter = setInterval(()=>{
			if(window.sscontact == 0){
				clearInterval(inter);
			}
  		else if(window.sscontact['First Name']){
  			if(window.sscontact['First Name'].split(' ').length>1){
  				window.sscontact['First Name'] = window.sscontact['First Name'].split(' ')[0];
	  		}
	  		this.setState({
	  			nombre: ' '+window.sscontact['First Name']
	  		});
	  		clearInterval(inter);
  		}
	  }, 1000);
	}

    
	render(){
		return(
			<div>
				<div id="vw_topzone">
					<p>Hola{this.state.nombre}:<br /><span style={{fontSize:'18px'}}>¿Quieres conocer cómo <strong>Vistage</strong> ha ayudado a miles de Gerentes Generales a mejorar los resultados de su empresa?</span></p>
				</div>
			</div>
		);
	}
}

export default TopZone;