import React, { Component } from 'react';

class ResponsiveStyles extends Component {
  render() {
    return (
      <style>{`
      	@media only screen and (max-width: 500px){
	      	#vw_messagebox{
	      		width: 90% !important;
	      	}
	      	#vw_bottomzone button{
	      		width:90% !important;
	      		margin:0 !important;
	      		margin-bottom:1em !important;
	      	}
	      	#vw_topzone p{
	      		font-size: 22px !important;
	      	}
      	}
      `}</style>
    );
  }
}

export default ResponsiveStyles;
