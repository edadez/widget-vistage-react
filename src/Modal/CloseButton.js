import React, { Component } from 'react';

class CloseButton extends Component{
	render(){
		return(
			<div id="vw_btnclosemodal" onClick={this.props.handlerClose}>
				<p></p>
			</div>
		);
	}
}

export default CloseButton;