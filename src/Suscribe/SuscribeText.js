import React, { Component } from 'react';

class SuscribeText extends Component {
  render() {
    return (
      <div id="vw_boxText">
        <h1>SUSCRÍBETE</h1>
        <p><small>y recibe interesantes artículos</small></p>
      </div>
    );
  }
}

export default SuscribeText;
