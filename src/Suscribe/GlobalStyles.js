import React, { Component } from 'react';

class GlobalStyles extends Component {
  render() {
    return (
      <style>{`
      	#vistageBox{
      		position: fixed !important;
		      width: 100% !important;
		      height: 60px;
		      background: #003f5b !important;
		      left: 0 !important;
		      transition: all 1s !important;
		      z-index: 99999999999999 !important;
      	}
        .vistageTab{
          display:none;
        }
      	.vistageTab:before{
          content: "• • •";
          font-size: 35px !important;
          color: black !important;
          text-align: center !important;
          width: 100px !important;
          height: 30px !important;
          position: absolute !important;
          top: -30px !important;
          left: 0 !important;
          right: 0 !important;
          margin: 0 auto !important;
          background: #003f5b !important;
          border-top-left-radius: 30px !important;
          border-top-right-radius: 30px !important;
          line-height: 1 !important;
          text-shadow: 1px 1px 1px grey !important;
          cursor:pointer !important;
        }
        #vw_boxText{
        	position: relative !important;
		      display: block !important;
		      -webkit-box-sizing: content-box !important;
			    -moz-box-sizing: content-box !important;
			    box-sizing: content-box !important;
		      width: 21%;
		      float: left !important;
		      color: white !important;
		      text-align: center !important;
		      line-height: 0.9 !important;
        }
        #vw_boxText h1{
        	padding: 0 !important;
      		margin: 0 !important;
      		color: white !important;
      		line-height: 0.9 !important;
      		font-size: 24px !important;
        }
        #vw_boxText p{
        	padding:0 !important;
        	margin:0 !important;
        	line-height: 1.5 !important;
        	font-size: 12px !important;
        }
        #vw_suscribeForm{
        	position: relative !important;
		      box-sizing: content-box !important;
		      width: 56%;
		      float: left !important;
		      display: block !important;
          border-right: 1px solid #9bd0b1;
          border-left: 1px solid #9bd0b1;
          padding-left: 10px;
        }
        #vw_suscribeForm form input{
        	border-radius: 3px !important;
		      padding: 10px !important;
		      margin: 0px 6px;
		      width: 28%;
		      height: 12px !important;
					background: white !important;
					-webkit-box-sizing: content-box !important;
			    -moz-box-sizing: content-box !important;
			    box-sizing: content-box !important;
        }
        #vw_suscribeForm form button{
        	background: orange !important;
		      color: white !important;
		      border: none !important;
		      width: 28%;
		      height: 36px !important;
		      margin: 0 6px !important;
		      cursor: pointer !important;
        }
        #vw_suscribeForm form button p{
          margin: 0;
          padding: 0;
          text-align: center;
          position: relative;
          margin-left: -11%;
        }
        #vw_suscribeForm form button span{
          margin-left: 6px;
          width: 15px;
          height: 15px;
          position: absolute;
          background-size: contain;
          max-width: 256px;max-height: 256px;background-image: url(data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjI0cHgiIGhlaWdodD0iMjRweCIgdmlld0JveD0iMCAwIDQzOC41MzMgNDM4LjUzMyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNDM4LjUzMyA0MzguNTMzOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxnPgoJPHBhdGggZD0iTTQzMS4zOTgsMjEwLjk4N0wzNjMuNDQ0LDUzLjM4OGMtMS45MDMtNC43NjItNS4zNzMtOC43NTctMTAuNDIxLTExLjk5MWMtNS4wNDEtMy4yMzktMTAuMDM3LTQuODU0LTE0Ljk4NS00Ljg1NEgxMDAuNDk5ICAgYy00Ljk0OSwwLTkuOTQ1LDEuNjE1LTE0Ljk4Nyw0Ljg1NGMtNS4wNDIsMy4yMzQtOC41Miw3LjIyOS0xMC40MjIsMTEuOTkxTDcuMTM3LDIxMC45ODdDMi4zNzgsMjIyLjU5OCwwLDIzNC4zMDIsMCwyNDYuMTAyICAgVjM4My43MmMwLDQuOTQ5LDEuODA3LDkuMjMsNS40MjQsMTIuODQ4YzMuNjE5LDMuNjEzLDcuOTAyLDUuNDI0LDEyLjg1MSw1LjQyNGg0MDEuOTkxYzQuOTQ4LDAsOS4yMjktMS44MTEsMTIuODQ3LTUuNDI0ICAgYzMuNjE0LTMuNjE3LDUuNDIxLTcuODk4LDUuNDIxLTEyLjg0OFYyNDYuMTAyQzQzOC41MzMsMjM0LjMwMiw0MzYuMTYxLDIyMi41OTgsNDMxLjM5OCwyMTAuOTg3eiBNMjkyLjA3MSwyMzcuNTM5bC0yNy4xMTMsNTQuODE5ICAgaC05MS4zNjdsLTI3LjEyMy01NC44MTlINTYuMjQ3YzAuMTkzLTAuMzgsMC40MjgtMS4xNCwwLjcxNS0yLjI4MmMwLjI4Ny0xLjE0LDAuNTI1LTEuOTAzLDAuNzE1LTIuMjgzbDYwLjUyNi0xNDEuNjA3aDIwMi4xMzggICBsNjAuNTIxLDE0MS42MDdjMC4xOTQsMC41NzUsMC40MzEsMS4zMzUsMC43MTEsMi4yODNjMC4yODgsMC45NDcsMC41MjQsMS43MDcsMC43MTksMi4yODJIMjkyLjA3MXoiIGZpbGw9IiNGRkZGRkYiLz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K)
        }
        #vw_suscribeClose{
        	position: relative;
			    display: block;
			   	-webkit-box-sizing: content-box !important;
			    -moz-box-sizing: content-box !important;
			    box-sizing: content-box !important;
			    width: 20%;
			    float: left;
          padding-left: 9px;
        }
        #vw_suscribeClose #btnCloseSuscribe{
        	background: #003f5b;
		      border: 1px solid lightgray;
		      color: lightgray;
          width: 78% !important;
          height: 36px !important;
          margin: 0 6px !important;
          cursor: pointer !important;
        }
        #vw_suscribeClose #btnCloseSuscribe:hover{
        	background:#045c83 !important;
        }
      `}</style>
    );
  }
}

export default GlobalStyles;
