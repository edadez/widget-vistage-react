import React, { Component } from 'react';

class SuscribeForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formDisplay: 'block',
      successDisplay: 'none'
    }
  }

  handlerSubmit = function(e){
    e.preventDefault();
    window.sendtoss();  
    this.setState({
      formDisplay: 'none',
      successDisplay: 'block'
    });
  }.bind(this) //MUY IMPORTANTE ESTO para poder settear los state

  render() {
    const formStyle = {
      display: this.state.formDisplay
    }
    const boxSuccessStyle = {
      color: 'white',
      textAlign: 'center',
      display: this.state.successDisplay
    }
    return (
      <div id="vw_suscribeForm">
        <form style={formStyle} method="get" id="awsuscribeform" onSubmit={this.handlerSubmit}>
          <input type="text" name="nombre" placeholder="Ingrese su nombre" required/>
          <input type="email" name="email" placeholder="Ingrese su email" required/>
          <style>{`#btnEnviarForm:hover{background: #bf7d05 !important;}`}</style>
          <button type="submit" id="btnEnviarForm"><p>SUSCRIBIRSE <span></span> </p></button>
        </form>
        <div style={boxSuccessStyle}>
          <p style={{margin:0,paddingTop:'10px'}}>Gracias! hemos recibido tus datos</p>
        </div>
      </div>
    );
  }
}

export default SuscribeForm;
