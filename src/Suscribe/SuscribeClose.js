import React, { Component } from 'react';

class SuscribeClose extends Component {
  render() {
    return (
      <div id="vw_suscribeClose">
        <button id="btnCloseSuscribe" onClick={this.props.setInvisible}>NO GRACIAS</button>
      </div>
    );
  }
}

export default SuscribeClose;
