import React, { Component } from 'react';

class ResponsiveStyles extends Component {
  render() {
    return (
      <style>{`
      	@media only screen and (max-width: 500px){
	        #vistageBox{
	        	height: 419px !important;
	        }
	        #vw_boxText{
	        	margin-top: 2em !important;
	        	width: 100% !important;
	        }
	        #vw_suscribeForm{
	        	margin-top: 1em !important;
	        	width: 100% !important;
	        	border-right: 0 !important;
	        	border-left: 0 !important;
	        	padding-left: 0;
	        	overflow: hidden !important;
	        }
	        #vw_suscribeForm input{
	        	width: 90% !important;
	        	margin-bottom:1em !important;
	        }
	        #vw_suscribeForm button{
	        	width: 100% !important;
	        }
	        #vw_suscribeClose{
	        	width: 100% !important;
	        	margin-top: 2em !important;
	        	text-align:center !important;
	        }
	        #vw_suscribeClose a{
						width: 54%;
						text-align: center;
						position: relative !important;
	        }
      	}
      `}</style>
    );
  }
}

export default ResponsiveStyles;
