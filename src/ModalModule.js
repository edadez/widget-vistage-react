import React, { Component } from 'react';
import GlobalStyles from './Modal/GlobalStyles';
import ResponsiveStyles from './Modal/ResponsiveStyles';
import MessageBox from './Modal/MessageBox';
import TopTrampa from './Modal/TopTrampa';

class ModalModule extends Component {

	constructor(props) {
	  super(props);
	
	  this.state = {
	  	showModal: 'none',
	  	opacity: '0',
	  	ocultarTrampa: 'block',
	  	active: false
		};
		
		// comentar lo arriba y descomentar esto para probar el modal 
		// this.state = {
	  // 	showModal: 'block',
	  // 	opacity: '1',
	  // 	ocultarTrampa: 'block',
	  // 	active: true
	  // };

	  var inter = setInterval(()=>{
      // console.log('sscontact:',window.sscontact);
      if(window.sscontact == 0){
        clearInterval(inter);
        this.setState({ active: true });
      }
      else if(window.sscontact != 0){ //si hay un objeto contacto (distinto a 0  (por defecto))
        if(!window.sscontact['Video Institucional']){ //si no se ha visto el vídeo
          this.setState({ active: true }); // Se activa el state
        }
        clearInterval(inter); //se deja de hacer el interval cada 2'
      }
    }, 2000);
	}

	handlerClose = function(){
		this.setState({opacity: '0'});
		setTimeout(()=>{
			this.setState({showModal: 'none'});
		}, 500);
	}.bind(this)

	mostrarModal = function(){
		if(this.state.active){
			setTimeout(()=>{
		  	this.setState({showModal: 'block', opacity:'0', ocultarTrampa: 'none'});
		  	setTimeout(()=>{
			  	this.setState({opacity:'1'});
			  }, 50)
		  }, 0) // TIEMPO DE ESPERA ANTES QUE SE LANCE LA MODAL ***********
		}
	}.bind(this)

  render() {
  	const modalBoxStyle = {
			display: this.state.showModal,
			opacity: this.state.opacity
		}
    return (
      <div>
      	<GlobalStyles />
      	<ResponsiveStyles />
      	<TopTrampa mostrarModal={this.mostrarModal} ocultar={this.state.ocultarTrampa}/>
        <div style={modalBoxStyle} className="modalBox" id="vw_modalbox">
          <MessageBox handlerClose={this.handlerClose}/>
        </div>
      </div>
    );
  }
}

export default ModalModule;
