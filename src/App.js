import React, { Component } from 'react';
import SuscribeModule from './SuscribeModule';
import ModalModule from './ModalModule';

class App extends Component {
  render() {
    return (
      <div>
	      <SuscribeModule />
	      <ModalModule />
      </div>
    );
  }
}

export default App;
